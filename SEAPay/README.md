# SEAPay Backend

- API design: https://hackmd.io/@FxGjeKhIQRClFZDv0wQvbQ/BkAF1VofS
- Ebean ORM API Documentation: https://ebean.io/apidoc/11/
- Play Framework API Documentation: https://www.playframework.com/documentation/2.7.x/api/java/index.html
- Play Framework Sample: https://github.com/playframework/play-samples

## TODO List

- [x] Database setup
- [x] Signin API (Need to implement JSON result)
- [x] Signup API (Need to implement JSON result)
- [x] Bcrypt encryption for Users model
- [ ] Transaction API
- [x] Transaction Service and Models
- [ ] Implement Voucher and Product System
- [ ] CSRF (Currently CSRF are disabled)
- [x] Login session
- [x] Session management (Able to access it).
- [x] Get user self information on `/api/user/me` (UserController.me)
- [x] Integrate testing
- [ ] Passed Gitlab CI
- [ ] Integrate website with Docker
- [ ] Integrate the website with React
- [ ] Implement logger
- [ ] Add logging to `models/Users.java` method `isPasswordEqual()`
- [ ] Finish up writing these TODO list