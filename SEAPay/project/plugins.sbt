addSbtPlugin("com.typesafe.play" % "sbt-plugin" % "2.7.3")
// Play Ebean
addSbtPlugin("com.typesafe.sbt" % "sbt-play-ebean" % "5.0.2")

//Test coverage
addSbtPlugin("org.scoverage" % "sbt-scoverage" % "1.6.0")
