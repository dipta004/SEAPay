package models.user;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import io.ebean.Finder;
import io.ebean.Model;
import models.user.serializer.CustomerSerializer;
import models.voucher.BoughtVoucher;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "customer")
@JsonSerialize(using = CustomerSerializer.class) // For Customing JSON Key
public class Customer extends Model implements UserDetail {
    @Id
    private Long customerId;

    private Long userId;

    private int seaPoint = 0;
    private int level = 0;

    private int loyalityPoint = 0;

    @Transient
    private transient List<BoughtVoucher> boughtVoucherList = new ArrayList<>();

    private static final Finder<Long, Customer> find = new Finder<>(Customer.class);

    public static Customer findByUserId(Long userId){
        return find.query().where().eq("user_id",userId).findOne();
    }

    public Customer(Long userId){
        this.userId = userId;
    }

    public void addSeaPoint(int seaPoint) {
        this.seaPoint += seaPoint;
    }

    public void setLevel(int level) {
        this.level = level;
    }

    public void addLoyalityPoint(int loyalityPoint) {
        this.loyalityPoint += loyalityPoint;
    }

    public Long getCustomerId() {
        return customerId;
    }

    public int getSeaPoint() {
        return seaPoint;
    }

    public int getLevel() {
        return level;
    }

    public int getLoyalityPoint() {
        return loyalityPoint;
    }

    public Long getUserId() {
        return userId;
    }

    public List<BoughtVoucher> getBoughtVoucherList() {
        return boughtVoucherList;
    }

    public void obtainVoucherList(){
        boughtVoucherList = BoughtVoucher.findVoucherByCustomerId(customerId);
    }

    public static Customer findCustomerDetailByUserId(Long userId){
        Customer customer = Customer.findByUserId(userId);
        customer.obtainVoucherList();

        return customer;
    }

}
