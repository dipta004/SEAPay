package models.user.serializer;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.ser.std.StdSerializer;
import models.user.Customer;

import java.io.IOException;

public class CustomerSerializer extends StdSerializer<Customer> {

    public CustomerSerializer(){
        this(null);
    }

    protected CustomerSerializer(Class<Customer> t) {
        super(t);
    }

    @Override
    public void serialize(Customer customer, JsonGenerator jsonGenerator, SerializerProvider serializerProvider) throws IOException {
        jsonGenerator.writeStartObject();
        jsonGenerator.writeNumberField("seaPoint",customer.getSeaPoint());
        jsonGenerator.writeNumberField("level",customer.getLevel());
        jsonGenerator.writeNumberField("loyalityPoint",customer.getLoyalityPoint());
        jsonGenerator.writeNumberField("customerId",customer.getCustomerId());
        jsonGenerator.writeObjectField("voucherList",customer.getBoughtVoucherList());
    }
}
