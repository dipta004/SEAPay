package models.user.serializer;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.ser.std.StdSerializer;
import models.user.Merchant;

import java.io.IOException;

public class MerchantSerializer extends StdSerializer<Merchant> {

    public MerchantSerializer(){
        this(null);
    }

    protected MerchantSerializer(Class<Merchant> t) {
        super(t);
    }

    @Override
    public void serialize(Merchant merchant, JsonGenerator jsonGenerator, SerializerProvider serializerProvider) throws IOException {
        jsonGenerator.writeStartObject();
        jsonGenerator.writeStringField("description",merchant.getDescription());
        if(merchant.getName() != null){
            jsonGenerator.writeStringField("name",merchant.getName());
        }
        if(merchant.getProductList() != null){
            jsonGenerator.writeObjectField("product_list",merchant.getProductList());
        }
        if(merchant.getWalletId() != null){
            jsonGenerator.writeNumberField("wallet_id",merchant.getWalletId());
        }
        jsonGenerator.writeNumberField("user_id",merchant.getUserId());
        jsonGenerator.writeNumberField("merchant_id",merchant.getMerchantId());
        jsonGenerator.writeEndObject();
    }
}
