package models.user.serializer;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.ser.std.StdSerializer;
import models.user.Admin;

import java.io.IOException;

public class AdminSerializer extends StdSerializer<Admin> {
    public AdminSerializer(){
        this(null);
    }

    protected AdminSerializer(Class<Admin> t) {
        super(t);
    }

    @Override
    public void serialize(Admin admin, JsonGenerator jsonGenerator, SerializerProvider serializerProvider) throws IOException {
        jsonGenerator.writeStartObject();
        jsonGenerator.writeObjectField("merchant_proposal",admin.getMerchantProposalList());
        jsonGenerator.writeObjectField("voucher_list",admin.getVoucherList());
        jsonGenerator.writeEndObject();
    }
}
