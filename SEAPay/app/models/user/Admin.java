package models.user;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import io.ebean.Finder;
import io.ebean.Model;
import models.transaction.Payment;
import models.user.serializer.AdminSerializer;
import models.voucher.Voucher;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "admin")
@JsonSerialize(using = AdminSerializer.class)
public class Admin extends Model implements UserDetail {
    @Id
    private Long adminId;

    private Long userId;
    private static final Finder<Long, Admin> find = new Finder<>(Admin.class);

    @Transient
    private transient List<Merchant> merchantProposalList = new ArrayList<>(0);

    @Transient
    private transient List<Voucher> voucherList;

    public Admin(Long userId){
        this.voucherList = new ArrayList<>();
        this.userId = userId;
    }

    public static Admin findByUserId(Long userId){
        return find.query().where().eq("user_id",userId).findOne();
    }

    public static Admin findAdminDetailByUserId(Long userId){
        Admin admin = find.query().where().eq("user_id",userId).findOne();
            admin.obtainMerchantProposalList();
            admin.obtainVoucherList();

        return admin;
    }

    public void obtainVoucherList(){
        voucherList = new ArrayList<>();
    }

    public void obtainMerchantProposalList(){
        merchantProposalList = Merchant.findMerchantProposal();
    }

    public List<Merchant> getMerchantProposalList() {
        return merchantProposalList;
    }

    public List<Voucher> getVoucherList() {
        return voucherList;
    }
}
