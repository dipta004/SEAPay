package models.transaction.builder;

import io.ebeaninternal.server.lib.util.Str;
import models.transaction.Payment;

import java.math.BigDecimal;

public class PaymentBuilder {
    private Long transactionId;
    private Long voucherId;
    private String voucherType;
    private BigDecimal realPrice;
    private BigDecimal customerPrice;
    private String merchantName;
    private Long merchantId;


    public PaymentBuilder setTransactionId(Long transactionId) {
        this.transactionId = transactionId;
        return this;
    }

    public PaymentBuilder setMerchantId(Long merchantId) {
        this.merchantId = merchantId;
        return this;
    }

    public PaymentBuilder setVoucherId(Long voucherId) {
        this.voucherId = voucherId;
        return this;
    }

    public PaymentBuilder setVoucherType(String voucherType) {
        this.voucherType = voucherType;
        return this;
    }

    public PaymentBuilder setRealPrice(BigDecimal realPrice) {
        this.realPrice = realPrice;
        return this;
    }

    public PaymentBuilder setCustomerPrice(BigDecimal customerPrice) {
        this.customerPrice = customerPrice;
        return this;
    }

    public PaymentBuilder setMerchantName(String merchantName) {
        this.merchantName = merchantName;
        return this;
    }


    public Payment createPayment() {
        return new Payment(transactionId, voucherId, voucherType, realPrice, customerPrice,merchantId, merchantName);
    }
}