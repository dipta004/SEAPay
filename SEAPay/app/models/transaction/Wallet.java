package models.transaction;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.google.gson.annotations.SerializedName;
import io.ebean.Finder;
import io.ebean.Model;
import models.transaction.serializer.WalletSerializer;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.math.BigDecimal;

@Entity
@Table(name = "wallet")
@JsonSerialize(using = WalletSerializer.class)
public class Wallet extends Model {

    @Id
    private Long walletId;
    private BigDecimal balance = new BigDecimal(0);
    private static final Finder<Long,Wallet> find = new Finder<>(Wallet.class);

    public Long getWalletId() {
        return walletId;
    }

    public BigDecimal getBalance() {
        return balance;
    }

    public void setBalance(BigDecimal balance) {
        this.balance = balance;
    }


    public static Wallet findById(Long walletId){
        return find.query().where().eq("wallet_id",walletId).findOne();
    }

    public void addBalance(BigDecimal balance){
        this.balance = this.balance.add(balance);
    }

    public void substractBalance(BigDecimal balance){
        this.balance = this.balance.subtract(balance);
    }
}
