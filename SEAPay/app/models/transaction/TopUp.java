package models.transaction;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import io.ebean.Finder;
import io.ebean.Model;
import models.transaction.serializer.TopUpSerializer;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.math.BigDecimal;

@Entity
@Table(name = "topup")
@JsonSerialize(using = TopUpSerializer.class)
public class TopUp extends Model implements TransactionDetail {

    @Id
    private Long topUpId;

    private Long transactionId;
    private BigDecimal ammount;

    private static final Finder<Long,TopUp> find = new Finder<>(TopUp.class);

    public TopUp(Long transactionId, BigDecimal ammount) {
        this.transactionId = transactionId;
        this.ammount = ammount;
    }

    public Long getTransactionId() {
        return transactionId;
    }

    public void setTransactionId(Long transactionId) {
        this.transactionId = transactionId;
    }

    public BigDecimal getAmmount() {
        return ammount;
    }

    public void setAmmount(BigDecimal ammount) {
        this.ammount = ammount;
    }

    public static TopUp findByTransactionId(Long transactionId){
        return find.query().where().eq("transaction_id",transactionId).findOne();
    }





}
