package models.transaction.serializer;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.ser.std.StdSerializer;
import models.transaction.Wallet;

import java.io.IOException;

public class WalletSerializer extends StdSerializer<Wallet> {
    public WalletSerializer(){
        this(null);
    }

    protected WalletSerializer(Class<Wallet> t) {
        super(t);
    }

    @Override
    public void serialize(Wallet wallet, JsonGenerator jsonGenerator, SerializerProvider serializerProvider) throws IOException {
        jsonGenerator.writeStartObject();
        jsonGenerator.writeNumberField("balance",wallet.getBalance().longValue());
        jsonGenerator.writeEndObject();
    }
}
