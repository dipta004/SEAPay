package models.transaction.serializer;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.ser.std.StdSerializer;
import models.transaction.Cashback;

import java.io.IOException;

public class CashbackSerializer extends StdSerializer<Cashback> {
    public CashbackSerializer(){
        this(null);
    }

    protected CashbackSerializer(Class<Cashback> t) {
        super(t);
    }

    @Override
    public void serialize(Cashback cashback, JsonGenerator jsonGenerator, SerializerProvider serializerProvider) throws IOException {
        jsonGenerator.writeStartObject();
        jsonGenerator.writeNumberField("ammount",cashback.getAmmount().longValue());
        jsonGenerator.writeNumberField("voucherId",cashback.getVoucherId().longValue());
        jsonGenerator.writeEndObject();
    }
}
