package models.transaction.serializer;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.ser.std.StdSerializer;
import models.transaction.TopUp;

import java.io.IOException;

public class TopUpSerializer extends StdSerializer<TopUp> {
    public TopUpSerializer(){
        this(null);
    }

    protected TopUpSerializer(Class<TopUp> t) {
        super(t);
    }

    @Override
    public void serialize(TopUp topUp, JsonGenerator jsonGenerator, SerializerProvider serializerProvider) throws IOException {
        jsonGenerator.writeStartObject();
        jsonGenerator.writeNumberField("ammount",topUp.getAmmount().longValue());
        jsonGenerator.writeEndObject();
    }
}
