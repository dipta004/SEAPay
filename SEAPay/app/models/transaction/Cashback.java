package models.transaction;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import io.ebean.Finder;
import io.ebean.Model;
import models.transaction.serializer.CashbackSerializer;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.math.BigDecimal;

@Entity
@Table(name = "cashback")
@JsonSerialize(using = CashbackSerializer.class)
public class Cashback extends Model implements TransactionDetail {

    @Id
    private Long cashbackId;


    private Long transactionId;
    private Long voucherId;
    private BigDecimal ammount;
    private static final Finder<Long,Cashback> find = new Finder<>(Cashback.class);


    public Cashback(Long voucherId, BigDecimal ammount, Long transactionId) {
        this.transactionId =transactionId;
        this.voucherId = voucherId;
        this.ammount = ammount;
    }

    public static Cashback findByTransactionId(Long transactionId){
        return find.query().where().eq("transaction_id",transactionId).findOne();
    }

    public Long getVoucherId() {
        return voucherId;
    }

    public void setVoucherId(Long voucherId) {
        this.voucherId = voucherId;
    }

    public BigDecimal getAmmount() {
        return ammount;
    }

    public void setAmmount(BigDecimal ammount) {
        this.ammount = ammount;
    }

    public void setTransactionId(Long transactionId) {
        this.transactionId = transactionId;
    }

    public Long getTransactionId() {
        return transactionId;
    }
}
