package models.voucher.builder;

import models.voucher.Voucher;

public class VoucherBuilder {
    private String name;
    private int value;
    private String description;
    private String type;
    private Long price;

    public VoucherBuilder setName(String name) {
        this.name = name;
        return this;
    }

    public VoucherBuilder setValue(int value) {
        this.value = value;
        return this;
    }

    public VoucherBuilder setDescription(String description) {
        this.description = description;
        return this;
    }

    public VoucherBuilder setType(String type) {
        this.type = type;
        return this;
    }

    public VoucherBuilder setPrice(Long price) {
        this.price = price;
        return this;
    }

    public Voucher createVoucher() {
        return new Voucher(name, value, description, type, price);
    }
}