package models.voucher.serializer;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.ser.std.StdSerializer;
import models.voucher.Voucher;

import java.io.IOException;

public class VoucherSerializer extends StdSerializer<Voucher> {

    public VoucherSerializer() {
        this(null);
    }

    protected VoucherSerializer(Class<Voucher> t) {
        super(t);
    }


    @Override
    public void serialize(Voucher voucher, JsonGenerator jsonGenerator, SerializerProvider serializerProvider) throws IOException {
        jsonGenerator.writeStartObject();
        jsonGenerator.writeNumberField("voucher_id",voucher.getVoucherId());
        jsonGenerator.writeStringField("name",voucher.getName());
        jsonGenerator.writeNumberField("value",voucher.getValue());
        jsonGenerator.writeEndObject();
    }
}
