package forms.builder;

import forms.PaymentForm;
import play.data.validation.Constraints;

import java.math.BigDecimal;

public class PaymentFormBuilder {
    @Constraints.Required()
    private Long customerId;

    @Constraints.Required()
    private Long customerWalletId;

    @Constraints.Required()
    private Long merchantWalletId;

    @Constraints.Required()
    private BigDecimal totalPrice;

    @Constraints.Required()
    private String merchantName;

    @Constraints.Required()
    private Long merchantId;

    private Long usedVoucherId;

    private String usedVoucherType;

    private int usedVoucherAmmount;

    public PaymentFormBuilder setCustomerId(Long customerId) {
        this.customerId = customerId;
        return this;
    }

    public PaymentFormBuilder setMerchantId(Long merchantId) {
        this.merchantId = merchantId;
        return this;
    }

    public PaymentFormBuilder setCustomerWalletId(Long customerWalletId) {
        this.customerWalletId = customerWalletId;
        return this;
    }

    public PaymentFormBuilder setMerchantWalletId(Long merchantWalletId) {
        this.merchantWalletId = merchantWalletId;
        return this;
    }

    public PaymentFormBuilder setTotalPrice(BigDecimal totalPrice) {
        this.totalPrice = totalPrice;
        return this;
    }

    public PaymentFormBuilder setUsedVoucherId(Long usedVoucherId) {
        this.usedVoucherId = usedVoucherId;
        return this;
    }

    public PaymentFormBuilder setUsedVoucherType(String usedVoucherType) {
        this.usedVoucherType = usedVoucherType;
        return this;
    }

    public PaymentFormBuilder setUsedVoucherAmmount(int usedVoucherAmmount) {
        this.usedVoucherAmmount = usedVoucherAmmount;
        return this;
    }

    public PaymentFormBuilder setMerchantName(String merchantName){
        this.merchantName = merchantName;
        return this;
    }

    public PaymentForm createPaymentForm() {
        return new PaymentForm(customerId, customerWalletId, merchantWalletId, totalPrice,merchantName,merchantId, usedVoucherId, usedVoucherType, usedVoucherAmmount);
    }
}