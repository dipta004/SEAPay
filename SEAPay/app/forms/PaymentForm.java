package forms;

import play.data.validation.Constraints;

import java.math.BigDecimal;
import java.util.List;

public class PaymentForm {
    @Constraints.Required()
    private Long customerId; // Contains userId

    @Constraints.Required()
    private Long customerWalletId;

    @Constraints.Required()
    private Long merchantWalletId;

    @Constraints.Required()
    private BigDecimal totalPrice;

    @Constraints.Required()
    private String merchantName;

    @Constraints.Required()
    private Long merchantId;

    @Constraints.Required
    private List<CartProductForm> productList;

    private Long usedVoucherId;

    private String usedVoucherType;
    
    private int usedVoucherAmmount;

    public PaymentForm(){}

    public PaymentForm(Long customerId, Long customerWalletId, Long merchantWalletId,  BigDecimal totalPrice, String merchantName, Long merchantId, Long usedVoucherId, String usedVoucherType, int usedVoucherAmmount) {
        this.customerId = customerId;
        this.customerWalletId = customerWalletId;
        this.merchantWalletId = merchantWalletId;
        this.totalPrice = totalPrice;
        this.usedVoucherId = usedVoucherId;
        this.usedVoucherType = usedVoucherType;
        this.usedVoucherAmmount = usedVoucherAmmount;
        this.merchantName = merchantName;
        this.merchantId = merchantId;
    }

    public Long getCustomerId() {
        return customerId;
    }

    public void setCustomerId(Long customerId) {
        this.customerId = customerId;
    }

    public Long getCustomerWalletId() {
        return customerWalletId;
    }

    public void setCustomerWalletId(Long customerWalletId) {
        this.customerWalletId = customerWalletId;
    }

    public Long getMerchantWalletId() {
        return merchantWalletId;
    }

    public void setMerchantWalletId(Long merchantWalletId) {
        this.merchantWalletId = merchantWalletId;
    }

    public BigDecimal getTotalPrice() {
        return totalPrice;
    }

    public void setTotalPrice(BigDecimal totalPrice) {
        this.totalPrice = totalPrice;
    }

    public Long getUsedVoucherId() {
        return usedVoucherId;
    }

    public void setUsedVoucherId(Long usedVoucherId) {
        this.usedVoucherId = usedVoucherId;
    }

    public String getUsedVoucherType() {
        return usedVoucherType;
    }

    public void setUsedVoucherType(String usedVoucherType) {
        this.usedVoucherType = usedVoucherType;
    }

    public int getUsedVoucherAmmount() {
        return usedVoucherAmmount;
    }

    public void setUsedVoucherAmmount(int usedVoucherAmmount) {
        this.usedVoucherAmmount = usedVoucherAmmount;
    }

    public String getMerchantName() {
        return merchantName;
    }

    public void setMerchantName(String merchantName) {
        this.merchantName = merchantName;
    }

    public Long getMerchantId() {
        return merchantId;
    }

    public void setMerchantId(Long merchantId) {
        this.merchantId = merchantId;
    }

    public List<CartProductForm> getProductList() {
        return productList;
    }

    public void setProductList(List<CartProductForm> productList) {
        this.productList = productList;
    }
}
