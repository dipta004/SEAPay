package forms;

import play.data.validation.Constraints;

public class AdminSignupForm {
    @Constraints.Required
    @Constraints.Email
    private String email;

    @Constraints.Required
    private String password;

    @Constraints.Required
    private String name;

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
