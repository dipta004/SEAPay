package forms;

import play.data.validation.Constraints;

import java.math.BigDecimal;

public class TopUpForm {
    @Constraints.Required()
    private Long walletId;

    @Constraints.Required()
    private BigDecimal ammount;

    public TopUpForm(){}

    public TopUpForm(@Constraints.Required() Long walletId, @Constraints.Required() BigDecimal ammount) {
        this.walletId = walletId;
        this.ammount = ammount;
    }

    public Long getWalletId() {
        return walletId;
    }

    public void setWalletId(Long walletId) {
        this.walletId = walletId;
    }

    public BigDecimal getAmmount() {
        return ammount;
    }

    public void setAmmount(BigDecimal ammount) {
        this.ammount = ammount;
    }
}
