package forms;

import play.data.validation.Constraints;

import javax.persistence.Id;
import java.math.BigDecimal;

public class TransferForm {
    @Id
    private Long transferId;

    @Constraints.Required()
    private Long walletId;

    @Constraints.Required()
    private BigDecimal ammount;

    @Constraints.Required()
    @Constraints.Email
    private String toAccountEmail;

    public TransferForm(){}

    public TransferForm(@Constraints.Required() Long walletId, @Constraints.Required() BigDecimal ammount, @Constraints.Required() @Constraints.Email String toAccountEmail) {
        this.walletId = walletId;
        this.ammount = ammount;
        this.toAccountEmail = toAccountEmail;
    }

    public Long getWalletId() {
        return walletId;
    }

    public void setWalletId(Long walletId) {
        this.walletId = walletId;
    }

    public BigDecimal getAmmount() {
        return ammount;
    }

    public void setAmmount(BigDecimal ammount) {
        this.ammount = ammount;
    }

    public String getToAccountEmail() {
        return toAccountEmail;
    }

    public void setToAccountEmail(String toAccountEmail) {
        this.toAccountEmail = toAccountEmail;
    }
}
