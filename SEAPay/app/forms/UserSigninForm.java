package forms;

import play.data.validation.Constraints;

import java.util.List;

public class UserSigninForm {
    @Constraints.Required()
    @Constraints.Email()
    private String email;
    
    @Constraints.Required()
    @Constraints.MinLength(6)
    //@Constraints.Pattern("")
    private String password;

    public String getEmail() {
        return this.email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return this.password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}