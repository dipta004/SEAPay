package service.product;

import forms.CartProductForm;
import forms.PaymentForm;
import forms.ProductCreationForm;
import models.product.Product;
import play.mvc.Result;
import util.ApiResponse;

import java.util.List;

import static play.mvc.Results.forbidden;
import static play.mvc.Results.ok;

public class ProductApplicationService {
    public static ApiResponse apiResponse = ApiResponse.getInstance();

    public static Result addProduct(ProductCreationForm productCreationForm){
        Product existingProduct = Product.findByProductNameWithMerchantId(
                                        productCreationForm.getName(),
                                        productCreationForm.getMerchantId());

        if(existingProduct != null){
            apiResponse.success = false;
            apiResponse.message = "Nama product sudah terpaakai";
            return forbidden(apiResponse.toJson());
        }

        apiResponse.success = true;
        apiResponse.data = ProductModelService.addProduct(productCreationForm);
        return ok(apiResponse.toJson());
    }

    public static Result removeProduct(Long productId){
        Product existingProduct = Product.findByProductId(productId);

        if(existingProduct == null){
            apiResponse.success = false;
            apiResponse.message = "Product tidak ada";
            return forbidden(apiResponse.toJson());
        }

        apiResponse.success = true;
        apiResponse.data = ProductModelService.removeProduct(productId);
        return ok(apiResponse.toJson());
    }

    public static Result updateProductStockFromCart(PaymentForm paymentForm){
        List<CartProductForm> cartProductFormList = paymentForm.getProductList();
        for (CartProductForm obj :
                cartProductFormList) {
            Product existingProduct = Product.findByProductId(obj.getProductId());

            if(existingProduct == null){
                apiResponse.success = false;
                apiResponse.message = "Product Id " + obj.getProductId().toString() + " tidak ada";
                return forbidden(apiResponse.toJson());
            }
        }

        ProductModelService.updateProductStockFromCart(cartProductFormList);

        apiResponse.success=true;
        return ok(apiResponse.toJson());
    }
}
