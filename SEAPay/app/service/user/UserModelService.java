package service.user;

import forms.AdminSignupForm;
import forms.MerchantForm;
import forms.UserSignupForm;
import models.user.Admin;
import models.user.Customer;
import models.user.Merchant;
import models.user.Users;
import models.transaction.Wallet;
import play.data.Form;

public class UserModelService {

    public static void createUser(UserSignupForm userSignupForm) {

        Wallet newWallet = new Wallet();
        newWallet.insert();

        Users newUser = new Users(newWallet.getWalletId());
        newUser.fromUserSignupForm(userSignupForm);
        newUser.insert();

        switch (newUser.getType().toLowerCase()){
            case "customer":
                UserModelService.createCustomer(newUser.getId());
                break;
            case "merchant":
                UserModelService.createMerchant(newUser.getId(),(MerchantForm) userSignupForm);
            default:
                break;
        }
    }

    public static void createCustomer(Long userId){
        Customer newCustomer = new Customer(userId);
        newCustomer.insert();
    }
    public static void createMerchant(Long userId,MerchantForm merchantForm){
        Merchant newMerchant = new Merchant(userId);
        newMerchant.setDescription(merchantForm.getDescription());
        newMerchant.insert();
    }

    public static void createAdmin(AdminSignupForm adminSignupForm){
        Users users = new Users(null);
            users.setType("Admin");
            users.setName(adminSignupForm.getName());
            users.setPassword(adminSignupForm.getPassword());
            users.setEmail(adminSignupForm.getEmail());

        users.insert();

        Admin admin = new Admin(users.getId());
        admin.insert();
    }


}
