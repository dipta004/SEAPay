package service.transaction;

import forms.PaymentForm;
import forms.TopUpForm;
import forms.TransferForm;
import models.transaction.Wallet;
import models.user.Users;
import play.data.Form;
import play.mvc.Http;
import play.mvc.Result;
import service.product.ProductApplicationService;
import util.ApiResponse;

import static play.mvc.Results.*;

public class TransactionApplicationService {

    public static ApiResponse apiResponse = ApiResponse.getInstance();

    public static Result payment(PaymentForm paymentForm){
        Users user = Users.findById(paymentForm.getCustomerId());

        if(user == null){
            apiResponse.success=false;
            apiResponse.message="User Not Found";
            return notFound(apiResponse.toJson());
        }

        Wallet customerWallet = Wallet.findById(paymentForm.getCustomerWalletId());

        if(customerWallet == null){
            apiResponse.success=false;
            apiResponse.message="Customer WalletId Not Found";
            return notFound(apiResponse.toJson());
        }

        if(customerWallet.getBalance().compareTo(paymentForm.getTotalPrice()) == -1){
            apiResponse.success=false;
            apiResponse.message="Balance Not Enough";
            return forbidden(apiResponse.toJson());
        }

        Users merchant = Users.findMerchantById(paymentForm.getMerchantId());

        if(merchant == null){
            apiResponse.success=false;
            apiResponse.message="Merchant Not Found";
            return notFound(apiResponse.toJson());
        }

        Wallet merchantWallet = Wallet.findById(paymentForm.getMerchantWalletId());

        if(merchantWallet == null){
            apiResponse.success=false;
            apiResponse.message="Merchant WalletId Not Found";
            return notFound(apiResponse.toJson());
        }

        //check and update stock product
        Result updateStockProductResult = ProductApplicationService.updateProductStockFromCart(paymentForm);
        if(updateStockProductResult.status() != Http.Status.OK){
            return updateStockProductResult;
        }

        TransactionModelService.payment(paymentForm);
        apiResponse.success=true;
        apiResponse.data = Wallet.findById(paymentForm.getCustomerWalletId());
        return ok(apiResponse.toJson());
    }

    public static Result topUp(TopUpForm topUpForm){
        Wallet wallet = Wallet.findById(topUpForm.getWalletId());

        if(wallet == null){
            apiResponse.success=false;
            apiResponse.message="WalletId Not Found";
            return notFound(apiResponse.toJson());
        }

        TransactionModelService.topUp(topUpForm);
        apiResponse.success=true;
        apiResponse.data = Wallet.findById(topUpForm.getWalletId());
        return ok(apiResponse.toJson());
    }

    public static Result transfer(TransferForm transferForm){
        Wallet wallet = Wallet.findById(transferForm.getWalletId());

        if (wallet == null){
            apiResponse.success=false;
            apiResponse.message="WalletId Not Found";
            return notFound(apiResponse.toJson());
        }

        if(wallet.getBalance().compareTo(transferForm.getAmmount()) == -1){
            apiResponse.success=false;
            apiResponse.message="Balance Not Enough";
            return forbidden(apiResponse.toJson());
        }

        Users users = Users.findByEmail(transferForm.getToAccountEmail());

        if(users == null){
            apiResponse.success=false;
            apiResponse.message="User Not Found";
            return notFound(apiResponse.toJson());
        }

        TransactionModelService.transfer(transferForm);
        apiResponse.success=true;
        apiResponse.data = Wallet.findById(transferForm.getWalletId());
        return ok(apiResponse.toJson());
    }

    public static Result historyTransaction(String email){
        Users users = Users.findByEmail(email);

        if(users == null){
            apiResponse.success=false;
            apiResponse.message="User Not Found";
            return notFound(apiResponse.toJson());
        }
        apiResponse.success=true;
        apiResponse.data = TransactionModelService.transactionHistory(users.getWalletId());
        return ok(apiResponse.toJson());
    }

    public static Result historyTransaction(Long walletId){
        Wallet wallet = Wallet.findById(walletId);

        if(wallet == null){
            apiResponse.success=false;
            apiResponse.message="Wallet Not Found";
            return notFound(apiResponse.toJson());

        }

        apiResponse.success=true;
        apiResponse.data = TransactionModelService.transactionHistory(walletId);
        return ok(apiResponse.toJson());
    }
}
