package service.transaction;

import forms.PaymentForm;
import forms.TopUpForm;
import forms.TransferForm;
import models.transaction.*;
import models.transaction.builder.PaymentBuilder;
import models.transaction.builder.TransactionBuilder;
import models.user.Customer;
import models.user.Users;

import java.math.BigDecimal;
import java.util.List;

public class TransactionModelService {

    public static void payment(PaymentForm paymentForm){
        //Not Done : Use Voucher function
        Transaction mainTransaction = new TransactionBuilder()
                .setCreditedWalletId(paymentForm.getMerchantWalletId())
                .setDebitedWalletId(paymentForm.getCustomerWalletId())
                .setType("Payment")
                .createTransaction();
        mainTransaction.insert();

        Wallet customerWallet = Wallet.findById(paymentForm.getCustomerWalletId());
        Wallet merchantWallet = Wallet.findById(paymentForm.getMerchantWalletId());

        if(paymentForm.getUsedVoucherType() != null){
            Payment payment = new PaymentBuilder()
                    .setRealPrice(paymentForm.getTotalPrice())
                    .setTransactionId(mainTransaction.getTransactionId())
                    .setVoucherId(paymentForm.getUsedVoucherId())
                    .setVoucherType(paymentForm.getUsedVoucherType())
                    .setMerchantName(paymentForm.getMerchantName())
                    .setMerchantId(paymentForm.getMerchantId())
                    .createPayment();

            if(paymentForm.getUsedVoucherType().equalsIgnoreCase("discount")){
                BigDecimal customerPrice = paymentForm.getTotalPrice().multiply(new BigDecimal((100 - paymentForm.getUsedVoucherAmmount())/100.0));
                payment.setCustomerPrice(customerPrice);
                payment.insert();

                customerWallet.substractBalance(customerPrice);
                merchantWallet.addBalance(payment.getRealPrice());

                customerWallet.update();
                merchantWallet.update();


            }else if (paymentForm.getUsedVoucherType().equalsIgnoreCase("cashback")){
                payment.setCustomerPrice(paymentForm.getTotalPrice());
                payment.insert();

                customerWallet.substractBalance(payment.getRealPrice());
                merchantWallet.addBalance(payment.getRealPrice());

                customerWallet.update();
                merchantWallet.update();

                TransactionModelService.cashback(paymentForm);
            }
        }
        else{
            Payment payment = new PaymentBuilder()
                    .setCustomerPrice(paymentForm.getTotalPrice())
                    .setRealPrice(paymentForm.getTotalPrice())
                    .setTransactionId(mainTransaction.getTransactionId())
                    .setMerchantName(paymentForm.getMerchantName())
                    .setMerchantId(paymentForm.getMerchantId())
                    .createPayment();

            payment.insert();

            customerWallet.substractBalance(payment.getRealPrice());
            merchantWallet.addBalance(payment.getRealPrice());

            customerWallet.update();
            merchantWallet.update();
        }

        Users buyer = Users.findByWalletId(paymentForm.getCustomerWalletId());
        Customer buyerCustomer = Customer.findByUserId(buyer.getId());

        //add loyality point and sea point
        buyerCustomer.addLoyalityPoint(paymentForm.getTotalPrice().intValue()/1000);
        buyerCustomer.addSeaPoint(paymentForm.getTotalPrice().intValue()/100);
        buyerCustomer.update();

    }

    public static void cashback(PaymentForm paymentForm){
        Transaction cashbackTransaction = new TransactionBuilder()
                .setCreditedWalletId(paymentForm.getCustomerWalletId())
                .setType("Cashback")
                .createTransaction();

        cashbackTransaction.insert();

        BigDecimal cashbackAmmount = paymentForm.getTotalPrice().multiply(new BigDecimal(paymentForm.getUsedVoucherAmmount()/100.0));

        Cashback cashback = new Cashback(paymentForm.getUsedVoucherId(),
                cashbackAmmount,
                cashbackTransaction.getTransactionId());
        cashback.insert();

        Wallet wallet = Wallet.findById(paymentForm.getCustomerWalletId());
        wallet.addBalance(cashbackAmmount);
        wallet.update();
    }

    public static void topUp(TopUpForm topUpForm){
        Transaction topUpTransaction = new TransactionBuilder()
                .setCreditedWalletId(topUpForm.getWalletId())
                .setType("TopUp")
                .createTransaction();

        topUpTransaction.insert();

        TopUp topUp = new TopUp(topUpTransaction.getTransactionId(),topUpForm.getAmmount());
        topUp.insert();

        Wallet wallet = Wallet.findById(topUpForm.getWalletId());

        wallet.addBalance(topUpForm.getAmmount());
        wallet.update();
    }

    public static void transfer(TransferForm transferForm){
        Users user = Users.findByEmail(transferForm.getToAccountEmail());
        Users from = Users.findByWalletId(transferForm.getWalletId());

        Transaction transferTransaction = new TransactionBuilder()
                .setCreditedWalletId(user.getWalletId())
                .setDebitedWalletId(transferForm.getWalletId())
                .setType("Transfer")
                .createTransaction();

        transferTransaction.insert();

        Transfer transfer = new Transfer(transferTransaction.getTransactionId(),transferForm.getAmmount(), transferForm.getToAccountEmail() ,from.getEmail() );
        transfer.insert();

        Wallet origin = Wallet.findById(transferForm.getWalletId());
        Wallet destination = Wallet.findById(user.getWalletId());

        origin.substractBalance(transferForm.getAmmount());
        destination.addBalance(transferForm.getAmmount());

        origin.update();
        destination.update();
    }

    public static List<Transaction> transactionHistory(Long walletId){
        return Transaction.findHistoryTransactionByWalletId(walletId);
    }

}
