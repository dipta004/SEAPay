package util;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.ser.std.StdSerializer;

import java.io.IOException;
import java.time.format.DateTimeFormatter;

public class ApiResponseSerializer extends StdSerializer<ApiResponse> {

    public ApiResponseSerializer(){
        this(null);
    }

    protected ApiResponseSerializer(Class<ApiResponse> t) {
        super(t);
    }

    @Override
    public void serialize(ApiResponse apiResponse, JsonGenerator jsonGenerator, SerializerProvider serializerProvider) throws IOException {

            jsonGenerator.writeStartObject();
            jsonGenerator.writeBooleanField("success",apiResponse.isSuccess());
            jsonGenerator.writeObjectField("data",apiResponse.getData());
            if(!apiResponse.isSuccess()){
                jsonGenerator.writeStringField("message",apiResponse.getMessage());
            }
            jsonGenerator.writeEndObject();
    }
}
