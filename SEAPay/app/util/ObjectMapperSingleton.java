package util;

import com.fasterxml.jackson.databind.ObjectMapper;

public class ObjectMapperSingleton extends ObjectMapper {
    private static ObjectMapperSingleton objectMapperSingleton = null;

    public static ObjectMapperSingleton getInstance(){
        if(objectMapperSingleton == null){
            objectMapperSingleton = new ObjectMapperSingleton();
        }
        return objectMapperSingleton;
    }
}
