package controllers;

import play.data.FormFactory;
import play.mvc.Controller;
import play.mvc.Result;
import play.mvc.Security;

import javax.inject.Inject;

import service.merchant.MerchantApplicationService;

import util.ApiResponse;

public class MerchantController extends Controller {
    private ApiResponse apiResponse = ApiResponse.getInstance();

    // GET /api/merchant
    @Security.Authenticated(Authentication.class)
    public Result merchantList(){
        return MerchantApplicationService.getMerchantList();
    }

    // GET /api/detail/merchant/:merchant_id
    @Security.Authenticated(Authentication.class)
    public Result merchantDetail(Long merchantId){
        return MerchantApplicationService.getMerchantDetail(merchantId);
    }

    // GET /api/merchant/accept/:merchant_id
    @Security.Authenticated(Authentication.class)
    public Result acceptMerchant(Long merchantId){
        return MerchantApplicationService.acceptMerchant(merchantId,session().get("email"),true);
    }

    // GET /api/merchant/decline/:merchant_id
    @Security.Authenticated(Authentication.class)
    public Result declineMerchant(Long merchantId){
        return MerchantApplicationService.acceptMerchant(merchantId,session().get("email"),false);
    }
}
