package controllers;

import play.mvc.Result;
import play.mvc.Http.Request;
import play.mvc.Security;

import java.util.Optional;

import util.ApiResponse;

// https://www.playframework.com/documentation/2.7.x/api/java/index.html
public class Authentication extends Security.Authenticator {
    
    // Find email in session
    @Override
    public Optional<String> getUsername(Request req) {
        return req.session().getOptional("email");
    }

    // If the getUsername() return an empty Optional then the user is unauthorized
    @Override
    public Result onUnauthorized(Request req) {
        ApiResponse apiResponse = ApiResponse.getInstance();
        apiResponse.success = false;
        apiResponse.message = "Not authenticated";
        return unauthorized(apiResponse.toJson());
    }

}