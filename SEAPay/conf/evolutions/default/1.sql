# --- Created by Ebean DDL
# To stop Ebean DDL generation, remove this comment and start using Evolutions

# --- !Ups

create table admin (
  admin_id                      bigserial not null,
  user_id                       bigint,
  constraint pk_admin primary key (admin_id)
);

create table boughtvoucher (
  bought_voucher_id             bigserial not null,
  customer_id                   bigint,
  voucher_id                    bigint,
  has_been_used                 boolean default false not null,
  constraint pk_boughtvoucher primary key (bought_voucher_id)
);

create table cashback (
  cashback_id                   bigserial not null,
  transaction_id                bigint,
  voucher_id                    bigint,
  ammount                       decimal(38),
  constraint pk_cashback primary key (cashback_id)
);

create table customer (
  customer_id                   bigserial not null,
  user_id                       bigint,
  sea_point                     integer not null,
  level                         integer not null,
  loyality_point                integer not null,
  constraint pk_customer primary key (customer_id)
);

create table merchant (
  merchant_id                   bigserial not null,
  user_id                       bigint,
  description                   varchar(255),
  has_seen_by_admin             boolean default false not null,
  is_accepted                   boolean default false not null,
  constraint pk_merchant primary key (merchant_id)
);

create table payment (
  payment_id                    bigserial not null,
  transaction_id                bigint,
  voucher_id                    bigint,
  voucher_type                  varchar(255),
  merchant_name                 varchar(255),
  merchant_id                   bigint,
  real_price                    decimal(38),
  customer_price                decimal(38),
  constraint pk_payment primary key (payment_id)
);

create table products (
  product_id                    bigserial not null,
  merchant_id                   bigint,
  name                          varchar(255),
  price                         decimal(38),
  description                   varchar(255),
  stock                         integer,
  constraint pk_products primary key (product_id)
);

create table topup (
  top_up_id                     bigserial not null,
  transaction_id                bigint,
  ammount                       decimal(38),
  constraint pk_topup primary key (top_up_id)
);

create table transaction (
  transaction_id                bigserial not null,
  credited_wallet_id            bigint,
  debited_wallet_id             bigint,
  created_at                    timestamptz not null,
  type                          varchar(255),
  constraint pk_transaction primary key (transaction_id)
);

create table transfer (
  transfer_id                   bigserial not null,
  transaction_id                bigint,
  ammount                       decimal(38),
  to_email                      varchar(255),
  from_email                    varchar(255),
  constraint pk_transfer primary key (transfer_id)
);

create table users (
  user_id                       bigserial not null,
  email                         varchar(255),
  password                      varchar(255),
  name                          varchar(255),
  type                          varchar(255),
  wallet_id                     bigint,
  constraint pk_users primary key (user_id)
);

create table voucher (
  voucher_id                    bigserial not null,
  name                          varchar(255),
  value                         integer not null,
  description                   varchar(255),
  type                          varchar(255),
  price                         bigint,
  constraint pk_voucher primary key (voucher_id)
);

create table wallet (
  wallet_id                     bigserial not null,
  balance                       decimal(38),
  constraint pk_wallet primary key (wallet_id)
);


# --- !Downs

drop table if exists admin cascade;

drop table if exists boughtvoucher cascade;

drop table if exists cashback cascade;

drop table if exists customer cascade;

drop table if exists merchant cascade;

drop table if exists payment cascade;

drop table if exists products cascade;

drop table if exists topup cascade;

drop table if exists transaction cascade;

drop table if exists transfer cascade;

drop table if exists users cascade;

drop table if exists voucher cascade;

drop table if exists wallet cascade;

