package service.transaction;

import com.typesafe.config.Config;
import com.typesafe.config.ConfigFactory;
import forms.PaymentForm;
import forms.TopUpForm;
import forms.TransferForm;
import forms.builder.PaymentFormBuilder;
import models.transaction.Wallet;
import models.user.Merchant;
import models.user.Users;
import org.junit.*;
import org.junit.runners.MethodSorters;
import play.Application;
import play.data.Form;
import play.db.Database;
import play.db.evolutions.Evolutions;
import play.mvc.Result;
import play.test.Helpers;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;

import static org.junit.Assert.*;
import static play.mvc.Http.Status.*;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class TransactionApplicationServiceTest {

    public static Application app;
    public static Database database;

    @Before
    public void setUpApp(){
        Config playConfig = ConfigFactory.load();
        Map<String, String> settings = new HashMap<String, String>();
        settings.put("db.default.driver", "org.postgresql.Driver");
        settings.put("db.default.url", playConfig.getString("db.default.url"));
        settings.put("db.default.username", "postgres");
        settings.put("db.default.password", "");
        settings.put("play.evolutions.autoApply", "true");
        settings.put("db.default.autoApply","true");
        settings.put("ebean.default", "models.*");
        app = play.test.Helpers.fakeApplication();
        database = app.injector().instanceOf(Database.class);
        Evolutions.applyEvolutions(database);
        Helpers.start(app);

        Wallet walletCustomer = new Wallet();
        Wallet walletMerchant = new Wallet();

        walletCustomer.setBalance(new BigDecimal(1000));
        walletCustomer.insert();

        walletMerchant.insert();

        Users customer = new Users(walletCustomer.getWalletId());
        customer.setEmail("dipta005@gmail.com");

        Users merchant = new Users(walletMerchant.getWalletId());
        merchant.setEmail("dipta004@gmail.com");
        merchant.setName("Anugerah 2.0");
        merchant.setType("Merchant");

        customer.insert();
        merchant.insert();
    }

    @Test
    public void ApaymentUserNotFound() {
        PaymentForm paymentForm = new PaymentFormBuilder()
                .setTotalPrice(new BigDecimal(1000))
                .setCustomerWalletId(Long.valueOf(1))
                .setMerchantWalletId(Long.valueOf(2))
                .setCustomerId(Long.valueOf(3))
                .setMerchantName("Anugerah 2.0")
                .createPaymentForm();

        Result result =  TransactionApplicationService.payment(paymentForm);

        assertEquals(NOT_FOUND,result.status());
    }

    @Test
    public void BpaymentNotEnoughBalance() {
        PaymentForm paymentForm = new PaymentFormBuilder()
                .setTotalPrice(new BigDecimal(2000))
                .setCustomerWalletId(Long.valueOf(1))
                .setMerchantWalletId(Long.valueOf(2))
                .setCustomerId(Long.valueOf(1))
                .createPaymentForm();

        Result result =  TransactionApplicationService.payment(paymentForm);

        assertEquals(FORBIDDEN,result.status());
    }

    @Test
    public void CpaymentCustomerWalletIdNotFound(){
        PaymentForm paymentForm = new PaymentFormBuilder()
                .setTotalPrice(new BigDecimal(1000))
                .setCustomerWalletId(Long.valueOf(3))
                .setMerchantWalletId(Long.valueOf(2))
                .setCustomerId(Long.valueOf(1))
                .setMerchantId(Long.valueOf(2))
                .setMerchantName("Anugerah 2.0")
                .createPaymentForm();

        Result result =  TransactionApplicationService.payment(paymentForm);

        assertEquals(NOT_FOUND,result.status());
    }

    @Test
    public void DpaymentMerchantWalletIdNotFound() {
        PaymentForm paymentForm = new PaymentFormBuilder()
                .setTotalPrice(new BigDecimal(1000))
                .setCustomerWalletId(Long.valueOf(1))
                .setMerchantWalletId(Long.valueOf(3))
                .setCustomerId(Long.valueOf(1))
                .setMerchantId(Long.valueOf(2))
                .setMerchantName("Anugerah 2.0")
                .createPaymentForm();

        Result result =  TransactionApplicationService.payment(paymentForm);

        assertEquals(NOT_FOUND,result.status());
    }

    @Test
    public void EpaymentMerchantNotFound() {
        PaymentForm paymentForm = new PaymentFormBuilder()
                .setTotalPrice(new BigDecimal(1000))
                .setCustomerWalletId(Long.valueOf(1))
                .setMerchantWalletId(Long.valueOf(2))
                .setCustomerId(Long.valueOf(1))
                .setMerchantId(Long.valueOf(3))
                .setMerchantName("Anugerah 3.0")
                .createPaymentForm();

        Result result =  TransactionApplicationService.payment(paymentForm);

        assertEquals(NOT_FOUND,result.status());
    }

    @Test
    public void FpaymentNoError() {
        PaymentForm paymentForm = new PaymentFormBuilder()
                .setTotalPrice(new BigDecimal(1000))
                .setCustomerWalletId(Long.valueOf(1))
                .setMerchantWalletId(Long.valueOf(2))
                .setCustomerId(Long.valueOf(1))
                .setMerchantId(Long.valueOf(2))
                .setMerchantName("Anugerah 2.0")
                .createPaymentForm();

        Result result =  TransactionApplicationService.payment(paymentForm);

        assertEquals(OK,result.status());
    }

    @Test
    public void GtopUpWalletIdNotFound(){
        TopUpForm topUpForm = new TopUpForm(Long.valueOf(3),new BigDecimal(1000));

        Result result = TransactionApplicationService.topUp(topUpForm);
        assertEquals(NOT_FOUND,result.status());
    }

    @Test
    public void HtopUpNoError(){
        TopUpForm topUpForm = new TopUpForm(Long.valueOf(1),new BigDecimal(1000));

        Result result = TransactionApplicationService.topUp(topUpForm);
        assertEquals(OK,result.status());
    }

    @Test
    public void ItransferWalletIdNotFound(){
        TransferForm transferForm = new TransferForm(Long.valueOf(3),new BigDecimal(1000),"dipta004@gmail.com");

        Result result = TransactionApplicationService.transfer(transferForm);
        assertEquals(NOT_FOUND,result.status());
    }

    @Test
    public void JtransferBalanceNotEnough(){
        TransferForm transferForm = new TransferForm(Long.valueOf(1),new BigDecimal(2000),"dipta004@gmail.com");

        Result result = TransactionApplicationService.transfer(transferForm);
        assertEquals(FORBIDDEN,result.status());
    }

    @Test
    public void KtransferDestinationNotFound(){
        TransferForm transferForm = new TransferForm(Long.valueOf(1),new BigDecimal(1000),"dipta006@gmail.com");

        Result result = TransactionApplicationService.transfer(transferForm);
        assertEquals(NOT_FOUND,result.status());
    }

    @Test
    public void LtransferNoError(){
        TransferForm transferForm = new TransferForm(Long.valueOf(1),new BigDecimal(1000),"dipta004@gmail.com");

        Result result = TransactionApplicationService.transfer(transferForm);
        assertEquals(OK,result.status());
    }

    @Test
    public void MhistoryTransactionUserNotFound(){
        Result result = TransactionApplicationService.historyTransaction(Long.valueOf(3));

        assertEquals(NOT_FOUND,result.status());
    }

    @Test
    public void NhistoryTransactionNoError(){
        Result result = TransactionApplicationService.historyTransaction(Long.valueOf(1));

        assertEquals(OK,result.status());
    }

    @After
    public void shutDownApp(){
        Evolutions.cleanupEvolutions(database);
        Helpers.stop(app);
    }
}
