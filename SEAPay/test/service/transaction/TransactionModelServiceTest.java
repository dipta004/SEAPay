package service.transaction;


import com.fasterxml.jackson.databind.ObjectMapper;
import com.typesafe.config.Config;
import com.typesafe.config.ConfigFactory;
import forms.PaymentForm;
import forms.ProductCreationForm;
import forms.TopUpForm;
import forms.TransferForm;
import forms.builder.PaymentFormBuilder;
import models.product.Product;
import models.transaction.*;
import models.user.Merchant;
import models.user.Users;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;
import play.Application;
import play.db.Database;
import play.db.evolutions.Evolutions;
import play.mvc.Http;
import play.test.Helpers;
import util.ObjectMapperSingleton;
import util.Singleton;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.stream.Collectors;

import static org.junit.Assert.*;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class TransactionModelServiceTest {

    public static Application app;
    public static Database database;


    @BeforeClass
    public static void setUpApp(){
        Config playConfig = ConfigFactory.load();
        Map<String, String> settings = new HashMap<String, String>();
        settings.put("db.default.driver", "org.postgresql.Driver");
        settings.put("db.default.url", playConfig.getString("db.default.url"));
        settings.put("db.default.username", "postgres");
        settings.put("db.default.password", "");
        settings.put("play.evolutions.autoApply", "true");
        settings.put("db.default.autoApply","true");
        settings.put("ebean.default", "models.*");
        app = play.test.Helpers.fakeApplication();
        database = app.injector().instanceOf(Database.class);
        Evolutions.applyEvolutions(database);
        Helpers.start(app);

        Wallet walletCustomer = new Wallet();
        Wallet walletMerchant = new Wallet();

        walletCustomer.setBalance(new BigDecimal(1000));
        walletCustomer.insert();

        walletMerchant.insert();

        Users user = new Users(walletMerchant.getWalletId());
        user.setEmail("dipta004@gmail.com");
        Users user2 = new Users(walletCustomer.getWalletId());
        user2.setEmail("dipta005@gmail.com");


        user.insert();
        user2.insert();
    }

    @Test
    public void ApaymentTransactionNoVoucher() {
        PaymentForm paymentForm = new PaymentFormBuilder()
                .setCustomerId(Long.valueOf(123))
                .setCustomerWalletId(Long.valueOf(1))
                .setMerchantWalletId(Long.valueOf(2))
                .setTotalPrice(new BigDecimal(1000))
                .setUsedVoucherType("Discount")
                .setMerchantName("Anugerah 2.0")
                .setMerchantId(Long.valueOf(2))
                .createPaymentForm();

        TransactionModelService.payment(paymentForm);

        List<Transaction> transaction = Transaction.findByCreditedWalletId(paymentForm.getMerchantWalletId());
        List<Transaction> transaction2 = Transaction.findByDebitedWalletId(paymentForm.getCustomerWalletId());

        Payment payment = Payment.findByTransactionId(transaction.get(transaction.size()-1).getTransactionId());

        Wallet walletCustomer = Wallet.findById(Long.valueOf(1));
        Wallet walletMerchant = Wallet.findById(Long.valueOf(2));

        assertNotEquals(0,transaction.size()); //Check there is new transaction
        assertNotEquals(0,transaction2.size());
        assertNotEquals(null,payment); //Check there is a payment
        assertEquals(paymentForm.getTotalPrice(),payment.getRealPrice()); //Check the real price
        assertEquals(paymentForm.getTotalPrice(),payment.getCustomerPrice());
        assertEquals(new BigDecimal(0),walletCustomer.getBalance()); //Check wallet balance
        assertEquals(new BigDecimal(1000),walletMerchant.getBalance());

        //Check time stamp
        assertNotEquals(null,transaction.get(transaction.size()-1).getCreatedDateTime());
        assertNotEquals(null,transaction2.get(transaction2.size()-1).getCreatedDateTime());

    }

    @Test
    public void BpaymentTransactionWith50DiscountVoucher() {

        Wallet walletCustomer2 = Wallet.findById(Long.valueOf(1)); //Reset the wallet balance
        Wallet walletMerchant2 = Wallet.findById(Long.valueOf(2));

        walletCustomer2.setBalance(new BigDecimal(1000));
        walletCustomer2.update();

        walletMerchant2.setBalance(new BigDecimal(0));
        walletMerchant2.update();

        PaymentForm paymentForm = new PaymentFormBuilder()
                .setCustomerId(Long.valueOf(123))
                .setCustomerWalletId(Long.valueOf(1))
                .setMerchantWalletId(Long.valueOf(2))
                .setTotalPrice(new BigDecimal(1000))
                .setUsedVoucherId(Long.valueOf(123123))
                .setUsedVoucherAmmount(50)
                .setUsedVoucherType("Discount")
                .setMerchantName("Anugerah 2.0")
                .setMerchantId(Long.valueOf(2))
                .createPaymentForm();

        TransactionModelService.payment(paymentForm);

        List<Transaction> transaction = Transaction.findByCreditedWalletId(paymentForm.getMerchantWalletId());

        Payment payment = Payment.findByTransactionId(transaction.get(transaction.size()-1).getTransactionId());

        Wallet walletCustomer = Wallet.findById(Long.valueOf(1));
        Wallet walletMerchant = Wallet.findById(Long.valueOf(2));

        assertEquals(paymentForm.getTotalPrice(),payment.getRealPrice()); //Check the real price
        assertEquals(new BigDecimal(500.0),payment.getCustomerPrice()); //Check discount price
        assertEquals(new BigDecimal(500),walletCustomer.getBalance()); //Check wallet balance
        assertEquals(new BigDecimal(1000),walletMerchant.getBalance());

        //Check time stamp
        assertNotEquals(null,transaction.get(transaction.size()-1).getCreatedDateTime());
    }

    @Test
    public void CpaymentTransactionWith40DiscountVoucher() {
        Wallet walletCustomer2 = Wallet.findById(Long.valueOf(1)); //Reset the wallet balance
        Wallet walletMerchant2 = Wallet.findById(Long.valueOf(2));

        walletCustomer2.setBalance(new BigDecimal(1000));
        walletCustomer2.update();

        walletMerchant2.setBalance(new BigDecimal(0));
        walletMerchant2.update();

        PaymentForm paymentForm = new PaymentFormBuilder()
                .setCustomerId(Long.valueOf(123))
                .setCustomerWalletId(Long.valueOf(1))
                .setMerchantWalletId(Long.valueOf(2))
                .setTotalPrice(new BigDecimal(1000))
                .setUsedVoucherId(Long.valueOf(123123))
                .setUsedVoucherAmmount(40)
                .setUsedVoucherType("Discount")
                .setMerchantName("Anugerah 2.0")
                .setMerchantId(Long.valueOf(2))
                .createPaymentForm();

        TransactionModelService.payment(paymentForm);

        List<Transaction> transaction = Transaction.findByCreditedWalletId(paymentForm.getMerchantWalletId());

        Payment payment = Payment.findByTransactionId(transaction.get(transaction.size()-1).getTransactionId());

        Wallet walletCustomer = Wallet.findById(Long.valueOf(1));
        Wallet walletMerchant = Wallet.findById(Long.valueOf(2));

        assertEquals(paymentForm.getTotalPrice(),payment.getRealPrice()); // Check the real price
        assertEquals(new BigDecimal(600),payment.getCustomerPrice());

        assertEquals(new BigDecimal(400),walletCustomer.getBalance()); // Check wallet balance
        assertEquals(new BigDecimal(1000),walletMerchant.getBalance());

        //Check time stamp
        assertNotEquals(null,transaction.get(transaction.size()-1).getCreatedDateTime());
    }

    @Test
    public void DpaymentTransactionWith30DiscountVoucher() {
        Wallet walletCustomer2 = Wallet.findById(Long.valueOf(1)); //Reset the wallet balance
        Wallet walletMerchant2 = Wallet.findById(Long.valueOf(2));

        walletCustomer2.setBalance(new BigDecimal(1000));
        walletCustomer2.update();

        walletMerchant2.setBalance(new BigDecimal(0));
        walletMerchant2.update();

        PaymentForm paymentForm = new PaymentFormBuilder()
                .setCustomerId(Long.valueOf(123))
                .setCustomerWalletId(Long.valueOf(1))
                .setMerchantWalletId(Long.valueOf(2))
                .setTotalPrice(new BigDecimal(1000))
                .setUsedVoucherId(Long.valueOf(123123))
                .setUsedVoucherAmmount(30)
                .setUsedVoucherType("Discount")
                .setMerchantName("Anugerah 2.0")
                .setMerchantId(Long.valueOf(2))
                .createPaymentForm();

        TransactionModelService.payment(paymentForm);

        List<Transaction> transaction = Transaction.findByCreditedWalletId(paymentForm.getMerchantWalletId());

        Payment payment = Payment.findByTransactionId(transaction.get(transaction.size()-1).getTransactionId());

        Wallet walletCustomer = Wallet.findById(Long.valueOf(1));
        Wallet walletMerchant = Wallet.findById(Long.valueOf(2));

        assertEquals(paymentForm.getTotalPrice(),payment.getRealPrice()); //Check the real price
        assertEquals(new BigDecimal(700),payment.getCustomerPrice());

        assertEquals(new BigDecimal(300),walletCustomer.getBalance()); // Check wallet balance
        assertEquals(new BigDecimal(1000),walletMerchant.getBalance());

        //Check time stamp
        assertNotEquals(null,transaction.get(transaction.size()-1).getCreatedDateTime());
    }

    @Test
    public void EpaymentTransactionWith50CashbackVoucher() {
        Wallet walletCustomer2 = Wallet.findById(Long.valueOf(1)); //Reset the wallet balance
        Wallet walletMerchant2 = Wallet.findById(Long.valueOf(2));

        walletCustomer2.setBalance(new BigDecimal(1000));
        walletCustomer2.update();

        walletMerchant2.setBalance(new BigDecimal(0));
        walletMerchant2.update();

        PaymentForm paymentForm = new PaymentFormBuilder()
                .setCustomerId(Long.valueOf(123))
                .setCustomerWalletId(Long.valueOf(1))
                .setMerchantWalletId(Long.valueOf(2))
                .setTotalPrice(new BigDecimal(1000))
                .setUsedVoucherId(Long.valueOf(123123))
                .setUsedVoucherAmmount(50)
                .setUsedVoucherType("Cashback")
                .setMerchantName("Anugerah 2.0")
                .setMerchantId(Long.valueOf(2))
                .createPaymentForm();

        TransactionModelService.payment(paymentForm);

        List<Transaction> transaction = Transaction.findByCreditedWalletId(paymentForm.getMerchantWalletId());

        Payment payment = Payment.findByTransactionId(transaction.get(transaction.size()-1).getTransactionId());

        List<Transaction> transactions = Transaction.findByCreditedWalletId(paymentForm.getCustomerWalletId());

        Cashback cashback = Cashback.findByTransactionId(transactions.get(transactions.size() - 1).getTransactionId());

        Wallet walletCustomer = Wallet.findById(Long.valueOf(1));
        Wallet walletMerchant = Wallet.findById(Long.valueOf(2));

        assertEquals(paymentForm.getTotalPrice(),payment.getRealPrice());
        assertEquals(paymentForm.getTotalPrice(),payment.getCustomerPrice()); //Check the price
        assertEquals(new BigDecimal(1000),walletMerchant.getBalance()); //Check merchant wallet balance

        //Cashback situation
        assertNotEquals(0,transactions.size()); //Check the customer get credited ammount
        assertNotEquals(null,cashback); //Check cashback has been created
        assertEquals(new BigDecimal(500),cashback.getAmmount()); //Check cashback ammount
        assertEquals(new BigDecimal(500),walletCustomer.getBalance()); //Check balance customer

        //Check time stamp
        assertNotEquals(null,transaction.get(transaction.size()-1).getCreatedDateTime());
        assertNotEquals(null,transactions.get(transactions.size()-1).getCreatedDateTime());
    }

    @Test
    public void FpaymentTransactionWith80CashbackVoucher() {
        Wallet walletCustomer2 = Wallet.findById(Long.valueOf(1)); //Reset the wallet balance
        Wallet walletMerchant2 = Wallet.findById(Long.valueOf(2));

        walletCustomer2.setBalance(new BigDecimal(1000));
        walletCustomer2.update();

        walletMerchant2.setBalance(new BigDecimal(0));
        walletMerchant2.update();

        PaymentForm paymentForm = new PaymentFormBuilder()
                .setCustomerId(Long.valueOf(123))
                .setCustomerWalletId(Long.valueOf(1))
                .setMerchantWalletId(Long.valueOf(2))
                .setTotalPrice(new BigDecimal(1000))
                .setUsedVoucherId(Long.valueOf(123123))
                .setUsedVoucherAmmount(80)
                .setUsedVoucherType("Cashback")
                .setMerchantName("Anugerah 2.0")
                .setMerchantId(Long.valueOf(2))
                .createPaymentForm();

        TransactionModelService.payment(paymentForm);

        List<Transaction> transaction = Transaction.findByCreditedWalletId(paymentForm.getMerchantWalletId());

        Payment payment = Payment.findByTransactionId(transaction.get(transaction.size()-1).getTransactionId());

        List<Transaction> customerTransaction = Transaction.findByCreditedWalletId(paymentForm.getCustomerWalletId());

        Cashback cashback = Cashback.findByTransactionId(customerTransaction.get(customerTransaction.size() - 1).getTransactionId());

        Wallet walletCustomer = Wallet.findById(Long.valueOf(1));

        assertEquals(paymentForm.getTotalPrice(),payment.getRealPrice());
        assertEquals(paymentForm.getTotalPrice(),payment.getCustomerPrice()); //Check the price

        //Cashback situations
        assertNotEquals(0,customerTransaction.size()); //Check the customer get credited ammount
        assertNotEquals(null,cashback); //Check cashback has been created
        assertEquals(new BigDecimal(800),cashback.getAmmount()); //Check cashback ammount
        assertEquals(new BigDecimal(800),walletCustomer.getBalance()); //Check balance customer

        //Check time stamp
        assertNotEquals(null,transaction.get(transaction.size()-1).getCreatedDateTime());
        assertNotEquals(null,customerTransaction.get(customerTransaction.size()-1).getCreatedDateTime());
    }

    @Test
    public void GtopUp1000() {
        Wallet walletCustomer2 = Wallet.findById(Long.valueOf(1)); //Reset the wallet balance

        walletCustomer2.setBalance(new BigDecimal(1000));
        walletCustomer2.update();

        TopUpForm topUpForm = new TopUpForm(Long.valueOf(1),new BigDecimal(1000));

        TransactionModelService.topUp(topUpForm);

        List<Transaction> transaction = Transaction.findByCreditedWalletId(topUpForm.getWalletId());

        Wallet walletCustomer = Wallet.findById(Long.valueOf(1));

        TopUp topUp = TopUp.findByTransactionId(transaction.get(transaction.size()-1).getTransactionId());

        assertEquals(new BigDecimal(2000),walletCustomer.getBalance());
        assertEquals("TopUp",transaction.get(transaction.size()-1).getType());

        assertNotEquals(null,topUp);
        assertEquals(new BigDecimal(1000),topUp.getAmmount());

        //Check time stamp
        assertNotEquals(null,transaction.get(transaction.size()-1).getCreatedDateTime());

    }

    @Test
    public void HtopUp1500() {
        Wallet walletCustomer2 = Wallet.findById(Long.valueOf(1)); //Reset the wallet balance

        walletCustomer2.setBalance(new BigDecimal(1000));
        walletCustomer2.update();

        TopUpForm topUpForm = new TopUpForm(Long.valueOf(1),new BigDecimal(1500));

        TransactionModelService.topUp(topUpForm);

        List<Transaction> transaction = Transaction.findByCreditedWalletId(topUpForm.getWalletId());

        Wallet walletCustomer = Wallet.findById(Long.valueOf(1));

        TopUp topUp = TopUp.findByTransactionId(transaction.get(transaction.size()-1).getTransactionId());

        assertEquals(new BigDecimal(2500),walletCustomer.getBalance()); //Check Balance
        assertEquals("TopUp",transaction.get(transaction.size()-1).getType()); //Check Type

        assertNotEquals(null,topUp);
        assertEquals(new BigDecimal(1500),topUp.getAmmount());

        //Check time stamp
        assertNotEquals(null,transaction.get(transaction.size()-1).getCreatedDateTime());
    }

    @Test
    public void Itransfer1000() {
        Wallet walletCustomer2 = Wallet.findById(Long.valueOf(1)); //Reset the wallet balance
        Wallet walletMerchant2 = Wallet.findById(Long.valueOf(2));

        walletCustomer2.setBalance(new BigDecimal(1000));
        walletCustomer2.update();

        walletMerchant2.setBalance(new BigDecimal(0));
        walletMerchant2.update();

        TransferForm transferForm = new TransferForm(Long.valueOf(1),new BigDecimal(1000),"dipta004@gmail.com");

        TransactionModelService.transfer(transferForm);

        Users user = Users.findByEmail("dipta004@gmail.com");

        Wallet walletDestination = Wallet.findById(user.getWalletId());
        Wallet walletOrigin = Wallet.findById(Long.valueOf(1));

        List<Transaction> destinationTransaction = Transaction.findByCreditedWalletId(user.getWalletId());
        List<Transaction> originTransaction = Transaction.findByDebitedWalletId(transferForm.getWalletId());

        Transfer transfer = Transfer.findByTransactionId(destinationTransaction.get(destinationTransaction.size()-1).getTransactionId());

        assertEquals("Transfer",destinationTransaction.get(destinationTransaction.size()-1).getType()); //Check the type
        assertEquals("Transfer",originTransaction.get(originTransaction.size()-1).getType());

        assertEquals(new BigDecimal(1000),walletDestination.getBalance());
        assertEquals(new BigDecimal(0),walletOrigin.getBalance());

        assertNotEquals(null,transfer);
        assertEquals(new BigDecimal(1000),transfer.getAmmount());

        //Check time stamp
        assertNotEquals(null,destinationTransaction.get(destinationTransaction.size()-1).getCreatedDateTime());
    }

    @Test
    public void Jtransfer500() {
        Wallet walletCustomer2 = Wallet.findById(Long.valueOf(1)); //Reset the wallet balance
        Wallet walletMerchant2 = Wallet.findById(Long.valueOf(2));

        walletCustomer2.setBalance(new BigDecimal(1000));
        walletCustomer2.update();

        walletMerchant2.setBalance(new BigDecimal(0));
        walletMerchant2.update();

        TransferForm transferForm = new TransferForm(Long.valueOf(1),new BigDecimal(500),"dipta004@gmail.com");

        TransactionModelService.transfer(transferForm);

        Users user = Users.findByEmail("dipta004@gmail.com");

        Wallet walletDestination = Wallet.findById(user.getWalletId());
        Wallet walletOrigin = Wallet.findById(Long.valueOf(1));

        List<Transaction> destinationTransaction = Transaction.findByCreditedWalletId(user.getWalletId());
        List<Transaction> originTransaction = Transaction.findByDebitedWalletId(transferForm.getWalletId());

        Transfer transfer = Transfer.findByTransactionId(destinationTransaction.get(destinationTransaction.size()-1).getTransactionId());

        assertEquals("Transfer",destinationTransaction.get(destinationTransaction.size()-1).getType()); //Check the type
        assertEquals("Transfer",originTransaction.get(originTransaction.size()-1).getType());

        assertEquals(new BigDecimal(500),walletDestination.getBalance());
        assertEquals(new BigDecimal(500),walletOrigin.getBalance());

        assertNotEquals(null,transfer);
        assertEquals(new BigDecimal(500),transfer.getAmmount());

        //Check time stamp
        assertNotEquals(null,destinationTransaction.get(destinationTransaction.size()-1).getCreatedDateTime());
    }

    @Test
    public void KtransactionHistory() {
        List<Transaction> history = TransactionModelService.transactionHistory(Long.valueOf(1));

        assertNotEquals(0,history.size());
        assertEquals("Transfer",history.get(0).getType());
        assertEquals("Payment",history.get(history.size()-1).getType());
    }


    @AfterClass
    public static void shutDownApp(){
        Evolutions.cleanupEvolutions(database);
        Helpers.stop(app);
    }
}
