SHELL=/bin/bash
SEAPAY_VERSION=0.0.1

# Uncomment and configure the postgres configuration below for local test
# POSTGRES_HOST = "localhost"
# POSTGRES_DB = "sea_pay"

# TODO: Implement another testing unit, like our react frontend testing
test: test-sbt

# Testing for backend
test-sbt: db-create
	bash -c "cd SEAPay/ && sbt clean coverage test"

db-drop:
	dropdb -h ${POSTGRES_HOST} --if-exists -Upostgres ${POSTGRES_DB}

# Drop database before setting up a new database, so it won't trying to
# overwrite the existing database
db-create: db-drop
	createdb -h ${POSTGRES_HOST} -Upostgres -Eutf8 ${POSTGRES_DB}

# Setup test for local environtment
local-test:
	bash -c "set -a && source test.env && cd SEAPay/ && sbt clean coverage test"

build: build-ui
	bash -c "cd SEAPay/ && sbt dist"

build-ui:
	bash -c "cd ui/ && npm run-script build"