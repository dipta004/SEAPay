import React,{Component} from 'react';
import { Link, NavLink } from 'react-router-dom';
import { Navbar, Nav, NavDropdown, Media, Row, Col} from 'react-bootstrap';
import Profile from './asset/blank-profile-picture-973460.svg';
import {connect} from 'react-redux';
import axios from 'axios';

class NavBar extends Component{
  logOut =() => {
    axios.get("/api/signout")
      .then(res => {
        this.props.logUserOut();
      })
      .catch(error => {
        console.log(error);
      })
  }
  render(){
    const {isLoggedIn, user} = this.props;
    const showIfLogin = (isTrue) => {
      if (isLoggedIn === isTrue){
        return { display: 'none' };
      }else{
        return { display: '' };
      }
    }
    return (
      <header style={{ marginBottom: (isLoggedIn ? 30 : 50), transition:"1s" }}>
        <Navbar collapseOnSelect expand="lg" bg="dark" variant="dark">
          <Navbar.Brand>
            <Link to="/" className="navbar-brand">
            <img
              src="/static/logo.png"
              width="30"
              height="30"
              className="d-inline-block align-top mr-2"
              alt="React Bootstrap logo"
            />
            {"SEA Pay"}
            </Link>
          </Navbar.Brand>
          <Navbar.Toggle aria-controls="responsive-navbar-nav" />
          <Navbar.Collapse id="responsive-navbar-nav" >
  
            <Nav style={showIfLogin(true)} className="ml-auto">
              <NavLink to="/signin" className="nav-link">Signin</NavLink>
              <NavLink to="/signup" className="nav-link">Signup</NavLink>
            </Nav>
            <Nav style={showIfLogin(false)} className="ml-auto">
              <NavLink to="/cart" className="nav-link">Cart</NavLink>
              <NavDropdown title={user} id="basic-nav-dropdown">
                <Media className="dropdown-item no-hover mb-3" style={{width:"15rem"}}>
                  <img
                    width={50}
                    height={50}
                    className="align-self-center"
                    src={Profile}
                    alt="Generic placeholder"
                    style={{borderRadius:"50%"}}
                  />
                    <Media.Body className="align-item-center" style={{transform:"translateY(4px)"}}>
                      <h6 className="text-muted dropdown-item no-hover mb-0">Halo,</h6>
                      <h6 className="dropdown-item no-hover" style={{fontWeight:"bold"}}>{user}</h6>
                    </Media.Body>
                </Media>
                  <Row className="px-0 mx-0 justify-content-center">  
                      <Col md={6} style={{borderRight:"lightgrey solid 1px",width:"25rem"}} className="px-1 justify-content-start">
                        <Link to="" className="dropdown-item hoverBgGray pl-1 pr-1">SEA Pay : <span style={{fontWeight:600}} className="float-right">Rp. {this.props.moneyBalance}</span></Link>
                        <h6 className="dropdown-item no-hover pl-1 pr-1">SEA Point : <span style={{fontWeight:600}} className="float-right">{this.props.pointBalance}</span></h6>
                      </Col>
                      <Col sm={5} style={{borderLeft:"lightgrey solid 1px"}} className="px-1 justify-self-center">
                          <Link to={"/profile/"+this.props.userId} className="dropdown-item px-2 hoverBgGray">Edit Profile</Link>
                          <Link to="/setting" className="dropdown-item px-2 hoverBgGray">Settings</Link>
                          <button onClick={this.logOut} className="dropdown-item px-2 hoverBgGray no-style-button">Logout</button>
                      </Col>
                  </Row>
  
              </NavDropdown>
            </Nav>
          </Navbar.Collapse>
        </Navbar>
      </header>
      );
    }
}

const mapStateToProps = (state) => {
    return {
      user:state.user_detail.name,
      userId:state.user_detail.user_id,
      moneyBalance:state.user_detail.balance,
      pointBalance:state.user_detail.sea_point
    }
}
const mapDispatchToProps = (dispatch) => {
  return {
    logUserOut:() => {dispatch({type:"LOG_USER_OUT"})}
  }
}

export default connect(mapStateToProps,mapDispatchToProps)(NavBar);