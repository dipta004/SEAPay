import React, { Component } from 'react';
import { Row, Card } from 'react-bootstrap';
import {VoucherList,AddVoucherCard,MerchantProposalListCard} from './index';
import {connect} from 'react-redux';

class AdminApp extends Component{
    removeVoucher = (id) => {
        const tempVoucherList = this.props.voucherList.filter(obj => {
            return obj.voucher_id !== id;
        })
        this.props.updateVoucherList(tempVoucherList);
    }
    addVoucher = (voucherObj) =>{
        const tempVoucherList = [...this.props.voucherList,voucherObj]
        this.props.updateVoucherList(tempVoucherList)
    }
    render(){
        const cardAdminDetailStyle = {
            "border" : "grey solid 1.2pt",
            "width":"24rem"};
        return(
            <Row id="admin-app" className="mt-4 justify-content-center">
                    <div className="px-0 mx-2">
                        <Card style={cardAdminDetailStyle} className="mx-auto">
                            <Card.Body className="px-3 pt-3 pb-3">
                                <Card.Title style={{"marginBottom" : 12,fontSize:"16pt"}}>{this.props.user}</Card.Title>
                                <Card.Subtitle>Admin</Card.Subtitle>
                            </Card.Body>
                        </Card>
                        <AddVoucherCard addVoucher={this.addVoucher} />
                    </div>
                    <div className="px-0 mx-2">
                        <h3 className="text-center">List Voucher</h3>
                        <VoucherList voucherList={this.props.voucherList} removeFunc={this.removeVoucher}/>
                    </div>
                    <div className="px-0 mx-2">
                        <h3 className="text-center">Merchant Proposal</h3>
                        <MerchantProposalListCard merchantProposal={this.props.merchantProposalList}/>
                    </div>
            </Row>
        );
    }
}


const mapStateToProps = (state) => {
    return {
        user:state.user_detail.name,
        voucherList:state.user_detail.voucher_list,
        merchantProposalList:state.user_detail.merchant_proposal_list
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        updateVoucherList:(obj) => {dispatch({type:"UPDATE_VOUCHER_LIST",detail:obj})},
        updateMerchantProposalList:(obj) => {dispatch({type:"UPDATE_MERCHANT_PROPOSAL_LIST",detail:obj})}
    }
}

export default connect(mapStateToProps,mapDispatchToProps)(AdminApp);