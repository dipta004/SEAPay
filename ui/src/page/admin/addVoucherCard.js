import React, { Component } from 'react';
import {Form,Card,Button, InputGroup, Row, Col} from 'react-bootstrap';

export default class AddVoucherCard extends Component{
    state={
        title:"",
        price:0,
        ammount:0,
        desc:"",
        type:"",
        validated:false
    }
    handleInput = (e) => {
        const isNumber = (text) => {return /^\d+$/.test(text);} //check number or not
        this.setState({
            [e.target.id] : (isNumber(e.target.value) ? parseInt(e.target.value) : e.target.value)
        })
    }
    handleSubmit = (e) => {
        const form = e.currentTarget;
        if (form.checkValidity() === false){
          e.preventDefault();
          e.stopPropagation();
          this.setState({
            validated: true
          })
        }else{
          e.preventDefault();
          e.stopPropagation();
          this.addVoucherCard();
          this.setState({
            title:"",
            price:0,
            ammount:0,
            desc:"",
            type:"",
            validated:false
            })
        }
      }
    addVoucherCard = () => {
        this.props.addVoucher({
            title:this.state.title,
            price:this.state.price,
            ammount:this.state.ammount,
            description:this.state.desc,
            type:this.state.type,
        })
    }
    render(){
        const cardAddVoucherStyle = {
            "border" : "grey solid 1.2pt",
            "width":"24rem"};
        return(
            <Card className = "mt-3 mx-auto" style={cardAddVoucherStyle}>
                <Card.Body className="pt-3">
                        <Card.Title className="text-center">Add Voucher</Card.Title>
                        <Form noValidate validated={this.state.validated} onSubmit={this.handleSubmit}  className="mt-2">
                            <Form.Group controlId="title">
                                <Form.Label>Title :</Form.Label>
                                <Form.Control required onChange={this.handleInput} type="text" placeholder="Voucher Title" value={this.state.title}/>
                                <Form.Control.Feedback type="invalid">Voucher title required!</Form.Control.Feedback>
                            </Form.Group>
                            <Form.Group controlId="desc">
                                <Form.Label>Description :</Form.Label>
                                <Form.Control required style={{height:"6rem"}} onChange={this.handleInput} as="textarea" type="text" placeholder="Description" value={this.state.desc}/>
                                <Form.Control.Feedback type="invalid">Voucher description required!</Form.Control.Feedback>
                            </Form.Group>
                            <Form.Group as={Row} controlId="type">
                                <Form.Label column sm="2" className="pr-0">Type :</Form.Label>
                                <Col sm="8" className="pl-2">
                                    <Form.Control required value={this.state.type} style={{background:"#eaecef"}} onChange={this.handleInput} as="select">
                                        <option value="" disabled hidden>Select a type</option>
                                        <option value="Discount">Discount</option>
                                        <option value="Cashback">Cashback</option>
                                    </Form.Control>
                                    <Form.Control.Feedback type="invalid">Select voucher type!</Form.Control.Feedback>
                                </Col>
                            </Form.Group>
                            <Form.Group as={Row} controlId="ammount">
                                <Form.Label column sm="4" className="pr-0">Ammount :</Form.Label>
                                <InputGroup as={Col} sm="5" className="pl-0">
                                    <Form.Control required type="number" min="1" max="100" onChange={this.handleInput} value={this.state.ammount} className="no-spin-button" />
                                    <InputGroup.Append>
                                        <InputGroup.Text style={{borderTopRightRadius:"5px",borderBottomRightRadius:"5px"}} id="basic-addon2">%</InputGroup.Text>
                                    </InputGroup.Append>
                                    <Form.Control.Feedback type="invalid">Ammount range is 1-100%!</Form.Control.Feedback>
                                </InputGroup>
                            </Form.Group>
                            <Form.Group as={Row} controlId="price">
                                <Form.Label column sm="3" className="pr-0">Price :</Form.Label>
                                <InputGroup as={Col} sm="7" className="pl-0">
                                        <Form.Control required type="number" min={1} className="no-spin-button" onChange={this.handleInput} value={this.state.price}/>
                                        <InputGroup.Append>
                                            <InputGroup.Text style={{borderTopRightRadius:"5px",borderBottomRightRadius:"5px"}} id="basic-addon2">SEA Point</InputGroup.Text>
                                        </InputGroup.Append>
                                        <Form.Control.Feedback type="invalid">Minimal price is 1 point!</Form.Control.Feedback>
                                </InputGroup>
                            </Form.Group>
                            <Button className="float-right" variant="primary" type="submit">
                                Add
                            </Button>
                        </Form>
                </Card.Body>
            </Card>
        );
    }
}
