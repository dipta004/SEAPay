import React,{Component} from 'react';
import { Card } from 'react-bootstrap';
import {MerchantProposalCard} from './index'

export default class MerchantProposalListCard extends Component{
    render(){
        const cardStyle = {
            width:"25rem",
            "border" : "grey solid 1.2pt",
            height:"40rem"
        }
        return(
            <Card style={cardStyle} className="mx-auto">
                <Card.Body>
                    <div className="scroll-hover" style={{height:"36rem", display:"flex", flexDirection:"column"}} >
                        {this.props.merchantProposal.map(obj => {
                            return (
                                <MerchantProposalCard merchantDetail={obj} key={obj.merchant_id} />
                            )
                        })}
                    </div>
                </Card.Body>
            </Card>
        );
    }
}