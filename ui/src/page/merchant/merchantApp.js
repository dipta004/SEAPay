import React,{Component} from 'react';
import { Row, Card } from 'react-bootstrap';
import {AddProductCard,ProductListCard} from './index'
import {connect} from 'react-redux';
import HistoryTransactionModal from './historyTransactionModal';
import axios from 'axios'

class MerchantApp extends Component{
    state = {
        showModal:false
    }
    setShowModal = (condition) => {
        this.setState({
            showModal:condition
        })
    }
    removeProduct = (id) => {
        axios.delete("/api/product/remove/"+id)
        .then(res => {
            this.props.updateProductList(res.data.data);
        })
        .catch(error => {
            console.log(error.response);
        })
    }
    render(){
        const cardUserDetailStyle = {
            "border" : "grey solid 1.2pt",
            "width":"24rem",
            margin:0};
        const historyButtonAction = () => {
            axios.get("/api/history")
                .then(res => {
                    this.props.updateTransactionHistory(res.data.data)
                    this.setShowModal(true);
                })
                .catch(error => {
                    console.log(error)
                })
        }
        return(
            <Row id="merchant-app" className="mt-4 justify-content-center">
                <div className="m-2">
                    <Card style={cardUserDetailStyle} className="mx-auto">
                        <Card.Body className="px-3 pt-3 pb-1">
                            <Card.Title style={{"marginBottom" : 12,fontSize:"16pt"}}>{this.props.name}</Card.Title>
                            <Card.Subtitle>Merchant</Card.Subtitle>
                            <Card.Text className="mt-3" style={{fontSize:"14pt"}}>
                                SEA Pay : Rp. {this.props.moneyBalance}
                            </Card.Text>
                        </Card.Body>
                        <Card.Body style={{fontWeight:"450",fontSize:"14pt"}}>
                            <Card.Text className="text-center hover-grey float-right"><button style={{fontWeight:500}} onClick={historyButtonAction} className="hover-grey no-style-button">History<br/>Transaction</button></Card.Text>
                        </Card.Body>
                    </Card>
                    <AddProductCard />
                </div>
                <div className="m-2">
                    <h3 className="text-center">Product List</h3>
                    <ProductListCard productList={this.props.productList} removeFunc={this.removeProduct}/>
                </div>
                <HistoryTransactionModal show={this.state.showModal} onHide={() => {this.setShowModal(false)}} />
            </Row>
        )
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        updateProductList: (obj) => {dispatch({type:"UPDATE_PRODUCT_LIST",detail:obj})},
        updateTransactionHistory : (obj) => {dispatch({type:"UPDATE_TRANSACTION_HISTORY",data:obj})}
    }
}

const mapStateToProps = (state) => {
    return {
        productList:state.user_detail.product_list,
        name:state.user_detail.name,
        moneyBalance:state.user_detail.balance,
    }
}

export default connect(mapStateToProps,mapDispatchToProps)(MerchantApp); 