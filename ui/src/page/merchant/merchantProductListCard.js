import React,{Component} from 'react';
import { Card } from 'react-bootstrap';
import { ProductCard} from './index'

export default class ProductListCard extends Component{
    render(){
        const productListStyle = {
            "border" : "grey solid 1.2pt",
            "width":"65.7rem",
            height:"40rem"
        };
        return(
            <Card style={productListStyle} className="mx-auto">
                <Card.Body>
                        <div className="scroll-hover" style={{width:"auto",height:"37rem",display:"flex",flexWrap:"wrap"}}>
                            {this.props.productList.map(obj => {
                                return(
                                        <ProductCard key={obj.product_id}  productDetail={obj} removeFunc={() => {this.props.removeFunc(obj.product_id)}} />                                    
                                );
                            })}
                        </div>
                </Card.Body>
            </Card>
        )
    }
}
