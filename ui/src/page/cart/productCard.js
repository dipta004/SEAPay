import React,{Component} from 'react';
import { Card,Row,Col,Image } from 'react-bootstrap';
import Plus from '../asset/baseline-add-24px.svg';
import Remove from '../asset/baseline-remove-24px.svg';
import {connect} from 'react-redux';
import ProductDetailModal from './productDetailModal';

class ProductCard extends Component {
        state = {
            showModal: false
        }
        addAmmount = () => {
                if(this.props.productObject.stock >= this.props.productDetail.ammount){
                this.props.updateCart({
                    ...this.props.productDetail,
                    totalPrice:(this.props.productDetail.ammount+1)*this.props.productDetail.price,
                    ammount:this.props.productDetail.ammount+1
                })
            }
        }
        reduceAmmount = () => {
            this.props.updateCart({
                ...this.props.productDetail,
                ammount:this.props.productDetail.ammount-1,
                totalPrice:(this.props.productDetail.ammount-1)*this.props.productDetail.price,
            })
        }
        render(){
            const {productDetail} = this.props;
            const cardStyle = {
                "border" : "grey solid 1.2pt",
                maxWidth:"36rem"
            }
            return(
                <div>
                    <Card className="mx-auto my-2" style={cardStyle}>
                        <Card.Body className="p-3">
                            <Row className="mb-1">
                                <Col xs={6}>
                                    <Card.Title><button onClick={() => {this.setState({showModal:true})}} className="no-style-button p-0 hover-grey" style={{fontWeight:"500"}}>{productDetail.name}</button></Card.Title>
                                </Col>
                                <Col xs={{span:3,offset:3}} className="pr-0">
                                    <h5 className="text-center">Rp. {productDetail.totalPrice}</h5>
                                </Col>
                            </Row>
                            <Row>
                                <Col xs={4} className="align-self-center">
                                    <Card.Subtitle  className="mt-0">@ Rp. {productDetail.price}</Card.Subtitle>
                                </Col>
                                <Col xs={{span:3,offset:5}}>
                                    <Row className="justify-content-center">
                                        <Col xs={3} className="p-0 text-center">
                                            <button onClick={this.reduceAmmount} className="no-style-button p-0 hoverable"><Image fluid src={Remove}/></button>
                                        </Col>
                                        <Col xs={3} className="p-0 align-self-center text-center">
                                            <h5 className="text-center mb-0">{productDetail.ammount}</h5>
                                        </Col>
                                        <Col xs={3} className="p-0 text-center">
                                            <button onClick={this.addAmmount} className="no-style-button p-0 hoverable"><Image fluid src={Plus}/></button>
                                        </Col>
                                    </Row>
                                </Col>
                            </Row>
                        </Card.Body>
                    </Card>
                    <ProductDetailModal productDetail={productDetail} show={this.state.showModal} onHide={() => {this.setState({showModal:false})}}/>
                </div>
            );
        }
}

const mapDispatchToProps = (dispatch) => {
    return {
        updateProduct:(obj) => {dispatch({type:"UPDATE_PRODUCT_STOCK",detail:obj})},
        updateCart:(obj) => {dispatch({type:"UPDATE_TO_CART",detail:obj})}
    }
}

const mapStateToProps = (state,ownProps) => {
    let productObject = state.merchant_detail.product_list.find(obj => obj.product_id === ownProps.productDetail.product_id);
    return {
        productObject : (productObject === undefined ? null : productObject)
    }
}

export default connect(mapStateToProps,mapDispatchToProps)(ProductCard);