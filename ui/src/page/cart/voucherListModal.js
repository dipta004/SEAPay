import React,{Component} from 'react';
import { Modal} from 'react-bootstrap';
import { VoucherCard } from '.';

export default class VoucherListModal extends Component{
    state={
        voucherList:[{
            name:"Voucher A",
            type:"Discount",
            ammount:50,
            voucher_id : 1
        },{
            name:'Voucher B',
            type:"Cashback",
            ammount:40,
            voucher_id:2
        },{
            name:'Voucher C',
            type:"Cashback",
            ammount:40,
            voucher_id:3
        },{
            name:'Voucher D',
            type:"Cashback",
            ammount:40,
            voucher_id:4
        },{
            name:'Voucher E',
            type:"Cashback",
            ammount:40,
            voucher_id:5
        },{
            name:'Voucher E',
            type:"Cashback",
            ammount:40,
            voucher_id:Math.random()
        },{
            name:'Voucher E',
            type:"Cashback",
            ammount:40,
            voucher_id:Math.random()
        },{
            name:'Voucher E',
            type:"Cashback",
            ammount:40,
            voucher_id:Math.random()
        },{
            name:'Voucher E',
            type:"Cashback",
            ammount:40,
            voucher_id:Math.random()
        },{
            name:'Voucher E',
            type:"Cashback",
            ammount:40,
            voucher_id:Math.random()
        },{
            name:'Voucher E',
            type:"Cashback",
            ammount:40,
            voucher_id:Math.random()
        },{
            name:'Voucher E',
            type:"Cashback",
            ammount:40,
            voucher_id:Math.random()
        },]
    }
    render(){
        return(
            <Modal show={this.props.show} onHide={this.props.onHide} scrollable dialogClassName="voucherListModal">
                <Modal.Header closeButton>
                    <Modal.Title style={{fontSize:"16pt"}}>My Voucher</Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    {this.state.voucherList.map(obj => {
                                return(
                                        <VoucherCard key={obj.voucher_id}  voucherDetail={obj} getVoucherDetailFunc={() => {this.props.getVoucherDetailFunc(obj)}} />                                    
                                );
                            })}
                </Modal.Body>
            </Modal>
        );
    }
}