import React,{useState} from 'react';
import { Card,Col,Image,Row } from "react-bootstrap";
import Plus from '../asset/baseline-add-24px.svg' ;
import VoucherCard from './voucherCard';
import VoucherShopModal from './voucherShopModal';

const VoucherList = ({voucherList}) => {
    const [ShowModal, setShowModal] = useState(false);
    const voucherListStyle = {
        "border" : "grey solid 1.2pt",
        "width":"25rem",
        height:"30rem"
    };
    const voucherTopStyle = {
        borderBottom : "#e0e0e0 solid 1.2pt",
    }
    return(
        <div>
            <Card style={voucherListStyle} className="m-auto">
                <Card.Body>
                    <Row style={voucherTopStyle}>
                        <Col md={6}>
                            <Card.Title>My Voucher</Card.Title>
                        </Col>
                        <Col md={{span: 2,offset:4}}>
                            <button onClick={() => {setShowModal(true)}} className="no-style-button p-0"><Image src={Plus} fluid width={30} height={30}/></button>
                        </Col>
                    </Row>
                    <div className="mt-2 scroll-hover" style={{height:"25rem", display:"flex", flexDirection:"column"}}>
                    {voucherList.map(obj => {
                        return(
                            <VoucherCard key={obj.voucher_id}  voucherDetail={obj} />                                    
                        );
                    })}
                    </div>
                </Card.Body>
            </Card>
            <VoucherShopModal show={ShowModal} onHide={() => {setShowModal(false)}} />
        </div>
    );
}
export default VoucherList;