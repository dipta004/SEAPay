import React,{Component} from 'react';
import { Container, Row, Card } from 'react-bootstrap';
import ProductCard from './userProductCard';
import {connect} from 'react-redux';
import Axios from 'axios';

class MerchantDetailPage extends Component {
    /*
     cart obj
     {
        name:
        price:
        ammount:
        totalPrice:
        stock:
        product_id:
     }
    */
    addItemToCart = (obj,id) => {
        const checkId = (id) => {
            const checker = this.props.cart.filter(object => {
                return id === object.product_id;
            })
            return checker.length > 0;
        }
        if(checkId(id)){
            this.props.updateToCart(obj)
        }else{
            this.props.addToCart(obj);
        }
    }
    componentDidMount(){  
        Axios.get("/api/detail/merchant/"+this.props.match.params.merchant_id)
        .then(res => {
            if((this.props.merchantId !== res.data.data.merchant_id) || (this.props.merchantId === undefined)){
                this.props.clearCart();
            }
            this.props.updateMerchantDetail(res.data.data);
        })
        .catch(error => {
            console.log(error.response)
        })
    }
    render(){
        const productListStyle = {
            "border" : "grey solid 1.2pt",
            "width":"65.7rem",
            height:"40rem"
        };
        const merchantDetailStyle ={
            "border" : "grey solid 1.2pt",
            "width":"24rem"
        }
        return(
            <Container fluid>
                <Row className="justify-content-center">
                    {this.props.productList !== undefined ?
                    //Has been Loaded
                    <div>
                        <div className="align-self-start m-2">
                            <Card style={merchantDetailStyle} className="mx-auto">
                                <Card.Body>
                                <h5 className="text-center mb-3">Merchant Detail</h5>
                                    <Card.Title>{this.props.name}</Card.Title>
                                    <Card.Subtitle>{this.props.description}</Card.Subtitle>
                                </Card.Body>
                            </Card>
                        </div>
                        <div className="align-self-center m-2">
                            <h3 className="text-center">Product List</h3>
                            <Card style={productListStyle} className="mx-auto">
                                <Card.Body>
                                    {this.props.productList.length !== 0 ? 
                                        //If there is a product
                                        <div className="scroll-hover" style={{width:"auto",height:"37rem",display:"flex",flexWrap:"wrap"}}>
                                            {this.props.productList.map(obj => {
                                                return(
                                                    <ProductCard key={obj.product_id} productDetail={obj} addToCart={this.addItemToCart}/>
                                                )
                                            })}
                                        </div>
                                    : 
                                            //If there is no product
                                            <h4 className="text-center">No Products</h4>
                                    }
                                </Card.Body>
                            </Card>
                        </div>
                    </div> : 

                    //Loading
                    <h3 className="text-center">Loading</h3> }
                </Row>
            </Container>
        )
    }
}

const mapStateToProps = (state) => {
    return {
        cart:state.cart,
        productList:state.merchant_detail.product_list,
        name:state.merchant_detail.name,
        description:state.merchant_detail.description,
        merchantId:state.merchant_detail.merchant_id
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        addToCart: (obj) => { dispatch({type:"ADD_TO_CART",detail:obj})},
        updateToCart : (obj) => { dispatch({type:"UPDATE_TO_CART",detail:obj})},
        updateMerchantDetail : (obj) => {dispatch({type:"UPDATE_MERCHANT_DETAIL",detail:obj})},
        clearCart:() => {dispatch({type:"CLEAR_CART"})}
    }
}

export default connect(mapStateToProps,mapDispatchToProps)(MerchantDetailPage);