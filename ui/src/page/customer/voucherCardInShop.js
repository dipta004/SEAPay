import React,{useState} from 'react';
import { Card,Row,Col,Container } from "react-bootstrap";
import VoucherDetailModal from './userVoucherDetailModalInShop';


const VoucherCard = ({voucherDetail,setParentModal}) => {
    const [ShowModal, setShowModal] = useState(false);
    const styleVoucher={
        "border" : "grey solid 1.2pt",
        borderRadius : "5px",
        "width":"21rem",
        height:"auto"};
    const wrappedShowModal = (state) => {
        if(state){
            setParentModal(true);
            setShowModal(true);
        }else{
            setParentModal(false);
            setShowModal(false);
        }
    }
    return(
        <div>
            <Card style={styleVoucher} className="mt-2 mx-auto">
                <Card.Body as={Container} className="p-3">
                    <Row className="mb-2">
                        <Col className="align-self-center mb-3">
                            <Card.Title className="mb-0"><button onClick={() => {wrappedShowModal(true)}} style={{fontWeight:"500"}} className="hover-grey no-style-button">{voucherDetail.title}</button></Card.Title>
                        </Col>
                    </Row>
                    <Row>
                        <Col className="pr-0 align-self-center">
                            <Card.Text>{voucherDetail.price} SEA Point</Card.Text>
                        </Col>
                        <Col className="pr-0 pl-5 align-self-center">
                            <Card.Subtitle className="align-self-center">{voucherDetail.type+" : "+voucherDetail.ammount+"%"}</Card.Subtitle>
                        </Col>
                    </Row>
                </Card.Body>
            </Card>
            <VoucherDetailModal show={ShowModal} onHide={() => {wrappedShowModal(false)}} voucherDetail={voucherDetail} />
        </div>
    );
}

export default VoucherCard;