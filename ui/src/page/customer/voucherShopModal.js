import React,{useState} from 'react';
import { Modal } from 'react-bootstrap';
import {connect} from 'react-redux';
import VoucherCard from './voucherCardInShop';

const VoucherShopModal = ({show,onHide,voucherList}) => {
    const [ModalShowed, setModalShowed] = useState(null);
    const setShowModal = (state) => {
        setModalShowed(state);
    }
    return (
        <Modal scrollable show={show} onHide={onHide} dialogClassName={ModalShowed ? "opacity-50" : ""}>
            <Modal.Header closeButton>
                <Modal.Title>Voucher Shop</Modal.Title>
            </Modal.Header>
            <Modal.Body>
                <div>
                    {voucherList.map(obj => <VoucherCard voucherDetail={obj} key={obj.voucher_id} setParentModal={setShowModal}/>)}
                </div>
            </Modal.Body>
        </Modal>
    )
}

const mapStateToProps = (state) => {
    return {
        voucherList : state.voucher_shop_list
    }
}

export default connect(mapStateToProps)(VoucherShopModal);