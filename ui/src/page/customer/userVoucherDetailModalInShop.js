import React from 'react';
import { Modal, Button } from 'react-bootstrap';

const VoucherDetailModal = ({voucherDetail,show,onHide}) => {
    const styleText ={
        fontWeight:"normal",
        overflowWrap:"break-word"}
    const buyFunc = () => {
        onHide();
    }
    return(
        <Modal show={show} onHide={onHide} aria-labelledby="contained-modal-title-vcenter"
      centered>
            <Modal.Header closeButton>
                <Modal.Title>{voucherDetail.title}</Modal.Title>
            </Modal.Header>
            <Modal.Body>
                <h4 style={styleText}>Type: {voucherDetail.type}</h4>
                <h4 style={styleText}>Ammount: {voucherDetail.ammount} %</h4>
                <h4 style={styleText}>Description: {voucherDetail.description}</h4>
            </Modal.Body>
            <Modal.Footer>
                <Button onClick={buyFunc} className="float-right">Buy</Button>
            </Modal.Footer>
        </Modal>
    )
}

export default VoucherDetailModal;